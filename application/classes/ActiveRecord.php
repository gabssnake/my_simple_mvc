<?php

class ActiveRecord {

    protected $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }


	// used by gateway
	public function has_attribute($attribute) {
		$object_vars = $this->attributes();
		return array_key_exists($attribute, $object_vars);
	}

	// used by internal functions to get attributes
	// does not alter the actual value of attributes
	protected function sanitized_attributes() {
		$clean_attributes = array();
		foreach ($this->attributes() as $key => $value) {
			$clean_attributes[$key] = $this->db->escape_value($value);
		}
		return $clean_attributes;
	}

	// subroutine
	// return an array of attribute keys and their values
	protected function attributes() {
		$attributes = array();
		foreach ($this->db_fields as $field) {
			if (property_exists($this, $field)) {
				$attributes[$field] = $this->$field;
			}
		}
		return $attributes;
	}


	
	// $user = $gateway->find_by_id(3);
	// $user->password = "hdiueouidhd";
	// $user->save();
	public function save() {
		// a new record wont have an id yet
		return isset($this->id) ? $this->update() : $this->create();
	}

	// $model = new UserGateway($db);
	// $user = $model->find_by_id(2);
	// if ($user) { $user->delete(); }
	public function delete() {
		$sql  = "DELETE FROM ". $this->table_name;
		$sql .= " WHERE id=". $this->db->escape_value($this->id);
		$sql .= " LIMIT 1";
		$this->db->query($sql);
		return ($this->db->affected_rows()==1) ? true : false;
	}

	// $user = new User();
	// $user->username   = "johnsmith";
	// $user->password   = "abcd12345";
	// $user->first_name = "John";
	// $user->last_name  = "Smith";
	// $user->save(); //calls create when user doesnt exists
	protected function create() {
		$attributes = $this->sanitized_attributes();
		$sql  = "INSERT INTO ". $this->table_name ." (";
		$sql .= join(", ", array_keys($attributes));
		$sql .= ") VALUES ('";
		$sql .= join("', '", array_values($attributes));
		$sql .= "')";
		if ($this->db->query($sql)) { 
			$this->id = $this->db->insert_id();
			return true; 
		} else { 
			return false; 
		}
	}

	// $user = $gateway->find_by_id(3);
	// $user->password = "abcdefghij";
	// $user->save(); //calls update when user exists
	protected function update() {
		$attributes = $this->sanitized_attributes();
		$attribute_pairs = array();
		foreach ($attributes as $key => $value) {
			$attribute_pairs[] = "{$key}='{$value}'";
		}
		$sql  = "UPDATE ". $this->table_name." SET ";
		$sql .= join(", ", $attribute_pairs);
		$sql .= " WHERE id=". $this->db->escape_value($this->id);
		$this->db->query($sql);
		return ($this->db->affected_rows()==1) ? true : false;
	}




}