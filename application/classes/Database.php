<?php

// database info
defined('DB_SERVER') ? null : define("DB_SERVER", "localhost");
defined('DB_USER')   ? null : define("DB_USER", "gallery");
defined('DB_PASS')   ? null : define("DB_PASS", "phpOTL123");
defined('DB_NAME')   ? null : define("DB_NAME", "photo_gallery");

defined('SITEROOT')  ? null : define('SITEROOT', dirname(APP_PATH) );


class Database {

	private $connection;
	private $magic_quotes;
	private $real_escape;
	public  $last_query;

	public function __construct() {
		$this->open_connection();
		$this->magic_quotes = get_magic_quotes_gpc();
		$this->real_escape = function_exists("mysql_real_escape_string"); // php >= 4.3
	}

	public function open_connection() {
		$this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
		if(!$this->connection) {
			die("Database connection failed: ".mysql_error());
		} else {
			$db_select = mysql_select_db(DB_NAME, $this->connection);
			if(!$db_select) {
				die("Database selection failed: ".mysql_error());
			}
		}
	}

	public function close_connection() {
		if(isset($this->connection)) {
			mysql_close($this->connection);
			unset($this->connection);
		}
	}


	public function query($sql) {
		$this->last_query = $sql;
		$result = mysql_query($sql, $this->connection);
		$this->confirm_query($result);
		return $result;
	}

	public function fetch_array($input) {
		return mysql_fetch_array($input);
	}

	public function num_rows($input) {
		return mysql_num_rows($input);
	}

	public function insert_id() {
		return mysql_insert_id($this->connection);
	}

	public function affected_rows() {
		return mysql_affected_rows($this->connection);
	}


	public function escape_value($value) {
		if ($this->real_escape) {
			if ($this->magic_quotes) { $value = stripslashes($value); }
			$value = mysql_real_escape_string($value);
		} else {
			if (!$this->magic_quotes) { $value = addslashes($value); }
		}
		return $value;
	}

	private function confirm_query($result) {
		if (!$result) {
			$output  = "Database query failed: ".mysql_error()."<br>";
			//$output .= "Last query was: " .$this->last_query;
			die($output);
		}
	}


}

// $db = new Database();

