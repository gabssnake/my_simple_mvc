<?php

class Autoload {

	protected $basepath;
	protected $paths = array();

	public function load($class) {
		// reverse to keep intuitive cascading
		$this->paths = array_reverse($this->paths);
		// go through each path
		foreach ($this->paths as $path) {
			$file =  $this->basepath . $path . $class.'.php';
			is_file($file) AND include_once $file;
		}
	}

	public function map($path, $ext='.php') {
		$this->paths[] = $path;
	}

	public function basepath($path) {
		$this->basepath = $path;
	}

}
