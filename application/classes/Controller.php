<?php

class Controller {

    public $view_path;
    public $log_path;

    public function __construct() {
        $this->view_path = APP_PATH.'/public/views/';
        $this->log_path  = APP_PATH.'/application/logs/log.txt';
    }

    protected function load_view($name, $data=null) {
       // make data available for view
       if( is_array($data) ) {
          extract($data);
       }
       // load view
       include $this->view_path.$name.'.php';
    }

    protected function redirect_to($location=null) {
        if($location!=null) {
            header('Location: '.WEB_PATH.$location);
            exit;
        }
    }

    protected function log_action($action, $message='') {
        $logfile = $this->log_path;
        $new = file_exists($logfile) ? false : true;
        if( $handle = fopen($logfile, 'a') ) {
            $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
            $content = "{$timestamp} - {$action} - {$message}\n";
            fwrite($handle, $content);
            fclose($handle);
            if ($new) { chmod($logfile, 0755); }
            return true;
        } else {
            // echo "Could not open log file.";
            return false;
        }
    }

    protected function nice_date($datetime="") {
        $unixdatetime = strtotime($datetime);
        return strftime("%B %d, %Y at %I:%M %p", $unixdatetime);
    }



}