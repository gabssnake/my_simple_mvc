<?php

class Session {

	protected $logged_in = false;

	public $user_id;
	public $message;

	public function __construct() {
		session_start();
		// avoid session fixation
		session_regenerate_id(true);
		// update from session
		$this->check_message();
		$this->check_login();
	}

	public function login($user) {
		if($user) {
			$this->user_id   = $_SESSION['user_id'] = $user->id;
			$this->logged_in = true;
		}
	}

	public function logout() {
		unset($_SESSION['user_id']);
		unset($this->user_id);
		$this->logged_in = false;
	}

	public function is_logged_in() {
		return $this->logged_in;
	}

	protected function check_login() {
		if (isset($_SESSION['user_id'])) {
			$this->user_id   = $_SESSION['user_id'];
			$this->logged_in = true;
		} else {
			unset($this->user_id);
			$this->logged_in = false;
		}
	}

	protected function check_message() {
		if (isset($_SESSION['message'])) {
			$this->message = $_SESSION['message'];
			unset($_SESSION['message']);
		} else {
			$this->message = '';
		}
	}

	// used as both getter and setter
	public function message($msg='') {
		if(!empty($msg)) {
			$_SESSION['message'] = $msg;
		} else {
			return $this->message;
		}
	}




}

