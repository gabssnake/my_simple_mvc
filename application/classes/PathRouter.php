<?php

// inspired by
// http://blog.sosedoff.com/2009/09/20/rails-like-php-url-router/

class PathRouter {

    protected $request  = "";
    protected $rules    = array();
    protected $params   = array();
    protected $action   = null;
    protected $fallback = null;
    protected $notfound = null;

    public function __construct() {
        $this->request = $this->get_request();
        $this->fallback($this->notfound);
    }

    // transform the human readable rule into a regex
    public function route($pattern, $action) {
        // ensure slash at the end
        $pattern = rtrim($pattern, '/').'/';
        
        // alphanumeric: <:var_name>
        $pattern = preg_replace('/\<\:(.*?)\>/', '(?P<\1>[A-Za-z0-9\-\_]+)', $pattern);
        
        // numeric: <#var_name>
        $pattern = preg_replace('/\<\#(.*?)\>/', '(?P<\1>[0-9]+)', $pattern);
        
        // anything ('/' included): <*var_name>
        $pattern = preg_replace( '/\<\*(.*?)\>/', '(?P<\1>.+)', $pattern);
        
        // forces a full match
        $pattern = '#^'.$pattern.'$#';
        // register the rule
        $this->rules[$pattern] = $action;
        return true;
    }

    public function monitor() {
        // reverse array for more intuitive rule priority
        // last declared rule has prevalence
        $this->rules = array_reverse($this->rules);
        // loop through each rule
        $match = false;
        foreach ($this->rules as $rule => $action) {
            if(preg_match($rule, $this->request, $fragments)) {
                $match = true;
                $params = array();
                // get named parameters
                foreach ($fragments as $key => $match) {
                    if (is_string($key)) {
                        $params[$key] = $match;
                    }
                }
                // execute route
                call_user_func_array($action, $params);
                // get out of loop so no other action are executed
                break;
            }
        }
        // if loop ended without matching any rule
        if (!$match) {
            // run fallback with all params in url
            $all_params = $this->extract();
            if ($this->fallback!==null) {
                call_user_func($this->fallback, $all_params);
            } else { $this->notfound(); }
        }
    }

    public function fallback($action) {
        $this->fallback = $action;
    }

    public function notfound() {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found!</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }

    protected function get_request() {
        // remove base folder
        $request = str_replace(dirname($_SERVER['SCRIPT_NAME']), '', $_SERVER['REQUEST_URI']);
        // remove GET vars
        $request = strtok($request, '?');
        // ensure slash at the end
        $request = rtrim($request, '/').'/';
        return $request;
    }

    protected function extract() {
        $request = $this->get_request();
        // create array of params
        $params  = explode("/", trim($request, '/'));
        // return array of all elements in url
        return $params;
    }

}




/*

// user controller 

class User {
    public function view($name) {
        echo "User view: {$name}!<br>";
    }
    public function edit($id) {
        echo "User edit: {$id}!<br>";
    }
}

$obj = new User();


// declare some routes

$router = new PathRouter();

// /user/view/gabs
$router->route('/user/view/<:username>', array($obj, 'view'));

// /user/edit/87
$router->route('/user/edit/<#userid>', array($obj, 'edit'));


$router->monitor();


// ---------------

$router = new PathRouter();

// basic
$router->route('/<:controller>', 'basic_route');
$router->route('/<:controller>/<:action>', 'basic_route');
$router->route('/<:controller>/<:action>/<:param1>', 'basic_route');
$router->route('/<:controller>/<:action>/<:param1>/<:param2>', 'basic_route');

// matches anything
$router->route('/<:controller>/<*wildcard>', 'basic_route');

// match alphanumeric parameter
$router->route('/user/view/<:username>', 'view_username');

// match numeric parameter
$router->route('/user/view/#user_id', 'number_username');

// replace fallback
$router->fallback('not_found');

// monitor rules
$router->monitor();


function basic_route($params) {
    echo '<br>basic_route<br>';
    if (empty($params)) { echo 'No params'; } 
    else{ echo '<pre>'; print_r($params); echo '</pre>'; }
}
function view_username($params=array()) {
    echo '<br>view user alphanumeric<br>';
    if (empty($params)) { echo 'No params'; } 
    else{ echo '<pre>'; print_r($params); echo '</pre>'; }
}
function number_username($params=array()) {
    echo '<br>view user number<br>';
    if (empty($params)) { echo 'No params'; } 
    else{ echo '<pre>'; print_r($params); echo '</pre>'; }
}
function not_found() {
    header('HTTP/1.0 404 Not Found');
    echo "<h1>Sorry, nothing found.</h1>";
    exit();
}


*/