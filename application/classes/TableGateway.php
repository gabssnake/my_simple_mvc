<?php

class TableGateway {
    
    protected $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }

	// object fabric
	// creates objects of the record class with provided data
	protected function fabricate($data) {
		$object = new $this->record_class($this->db);
		foreach ($data as $attribute => $value) {
			if ($object->has_attribute($attribute)) {
				$object->$attribute = $value;
			}
		}
		return $object;
	}

	// main database routine
	// uses factory to return object array
	protected function find_by_sql($sql) {
		$result_set = $this->db->query($sql);
		$object_array = array();
		while ($row = $this->db->fetch_array($result_set)) {
			$object_array[] = $this->fabricate($row);
		}
		return $object_array;
	}

	// $model = new UserGateway($db);
	// $users = $model->find_all();
	// foreach ($users as $user) {
	// 	echo "User: ". $user->username ."<br>";
	// 	echo "Name: ". $user->full_name() ."<br><br>";
	// }
	// returns an array of objects
	public function find_all() {
		return $this->find_by_sql("SELECT * FROM ". $this->table_name);
	}

	// $model = new UserGateway($db);
	// $user = $model->find_by_id(28);
	// return just an object
	public function find_by_id($id=0) {
		$id = $this->db->escape_value($id);
		$result_array = $this->find_by_sql("SELECT * FROM ".$this->table_name." WHERE id={$id} LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}

}