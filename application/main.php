<?php

class Main {

	protected $db;
	protected $session;
	protected $member;
	protected $gallery;
	protected $backend;
	protected $logger;
	protected $router;


	public function __construct() {

		// we'll need these
		$this->setup_paths();

		// start database and session
		$this->db = new Database();
		$this->session = new Session();

		// controllers
		$this->member  = new Member($this->db, $this->session);
		$this->gallery = new Gallery($this->db, $this->session);
		$this->backend = new Backend($this->db, $this->session);
		$this->logger  = new Logger($this->db, $this->session);

		// start router
		$this->router = new PathRouter();
		$this->setup_routes();
		$this->router->monitor();
	}


	protected function setup_paths() {

		// application paths
		$app_path = dirname(dirname(__FILE__));
		$web_path = 'http://'.$_SERVER['HTTP_HOST'].dirname(dirname($_SERVER['PHP_SELF'])).'/public';

		// autload classes
		require $app_path.'/application/classes/Autoload.php';
		$loader = new Autoload();
		// paths base
		$loader->basepath($app_path.'/application');
		// map some paths
		$loader->map('/'); // controllers
		$loader->map('/models/');
		$loader->map('/classes/');
		// register it
		spl_autoload_register(array($loader, 'load'));

		// too lasy for dependency injection, so using named constants
		defined('WEB_PATH') ? null : define('WEB_PATH', $web_path );
		defined('APP_PATH') ? null : define('APP_PATH', $app_path );
	}


	protected function setup_routes() {
		// shorter
		$r = $this->router;

		// routes for public site controller
		$r->route('/', array($this->gallery, 'all'));
		$r->route('/view/all', array($this->gallery, 'all'));
		$r->route('/view/<#photoid>', array($this->gallery, 'details'));
		$r->route('/comment/<#photoid>', array($this->gallery, 'comment'));

		// routes for auth controller
		$r->route('/admin/login', array($this->member, 'login'));
		$r->route('/admin/logout', array($this->member, 'logout'));

		// routes for backend controller
		$r->route('/admin/', array($this->backend, 'main'));
		$r->route('/admin/library', array($this->backend, 'library'));
		$r->route('/admin/comments/<#photoid>', array($this->backend, 'comments'));
		$r->route('/admin/comment/delete/<#commentid>', array($this->backend, 'delete_comment'));
		$r->route('/admin/delete/<#photoid>', array($this->backend, 'delete_photo'));
		$r->route('/admin/upload', array($this->backend, 'upload'));

		// routes for log controller
		$r->route('/admin/log', array($this->logger, 'show_log'));
		$r->route('/admin/log/clear', array($this->logger, 'clear_log'));
	}



}