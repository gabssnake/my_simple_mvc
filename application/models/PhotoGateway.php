<?php

class PhotoGateway extends TableGateway {

	protected $table_name   = "photographs";
	protected $record_class = 'Photo';

    public function __construct(Database $db) {
        parent::__construct($db);
    }


}