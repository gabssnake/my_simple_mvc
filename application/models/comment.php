<?php

class Comment extends ActiveRecord {

	protected $table_name = "comments";
	protected $db_fields  = array('id','photograph_id','created','author','body');

	public $id;
	public $photograph_id;
	public $created;
	public $author;
	public $body;

    public function __construct(Database $db) {
        parent::__construct($db);
    }

}