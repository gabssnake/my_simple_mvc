<?php

class CommentGateway extends TableGateway {

	protected $table_name   = "comments";
	protected $record_class = 'Comment';

    public function __construct(Database $db) {
        parent::__construct($db);
    }


	// factory
	public function make($photo_id, $author="Anonymous", $body="") {
		if( !empty($photo_id) && !empty($author) && !empty($body) ) {
			$comment = new $this->record_class($this->db);
			$comment->photograph_id = (int)$photo_id;
			$comment->created       = strftime("%Y-%m-%d %H:%M:%S", time());
			$comment->author        = $author;
			$comment->body          = $body;
			return $comment;
		} else {
			return false;
		}
	}

	// get comments for photo
	public function find_comments_on($photo_id=0) {
		$sql  = "SELECT * FROM ". $this->table_name;
		$sql .= " WHERE photograph_id=". $this->db->escape_value($photo_id);
		$sql .= " ORDER BY created ASC";
		return $this->find_by_sql($sql);
	}


}