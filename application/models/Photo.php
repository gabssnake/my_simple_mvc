<?php

class Photo extends ActiveRecord {

	protected $table_name = "photographs";
	protected $db_fields  = array('id','filename','type','size','caption');
	protected $comment_model;

	protected $site_root  = SITEROOT;

	protected $upload_dir;

	public $id;
	public $filename;
	public $type;
	public $size;
	public $caption;

	public $errors = array();


	protected $temp_path;
	protected $upload_errors = array(
		UPLOAD_ERR_OK			=>	"No errors.",	
		UPLOAD_ERR_INI_SIZE		=>	"File was too large (php settings).",
		UPLOAD_ERR_FORM_SIZE	=>	"File was too large (form settings).",
		UPLOAD_ERR_PARTIAL		=>	"Only part of the file was uploaded.",
		UPLOAD_ERR_NO_FILE		=>	"No file was found.",
		UPLOAD_ERR_NO_TMP_DIR	=>	"No temporary directory available.",
		UPLOAD_ERR_CANT_WRITE	=>	"Could not write to temporary folder.",
		UPLOAD_ERR_EXTENSION	=>	"The upload was stopped by some extension."
	);

    public function __construct(Database $db) {
        parent::__construct($db);
		$this->comment_model = new CommentGateway($this->db);
		$this->upload_dir    = APP_PATH.'/public/assets/img';
		$this->web_path      = WEB_PATH.'/assets/img';
    }



	// receives: $_FILES(['file_upload']) as argument

	public function attach_file($file) {
		// nothing uploaded or wrong argument passed
		if (!$file || empty($file) || !is_array($file)) {
			$this->errors[] = "No file was uploaded.";
			return false;
		} elseif ($file['error'] != 0) {
			// pass error that was found
			$this->errors[] = $this->upload_errors[$file['error']];
			return false;
		} else {
			// set object attrubutes from passed values
			$this->temp_path = $file['tmp_name'];
			$this->filename  = basename($file['name']);
			$this->type      = $file['type'];
			$this->size      = $file['size'];
			return true;
		}
	}


	// save() overrides 'ActiveRecord' definition

	public function save() {

		if(isset($this->id)) {
			$this->update(); 
		} else {

			// check for errors
			if(!empty($this->errors)) { 
				return false;
			}
			if(strlen($this->caption) > 255) {
				$this->errors[] = "The caption can be upto 255 characters long.";
				return false;
			}
			if(empty($this->filename) || empty($this->temp_path)) {
				$this->errors[] = "The file location was not available.";
				return false;
			}
			
			$target_path = $this->upload_dir."/".$this->filename;

			if (file_exists($target_path)) {
				$this->errors[] = "The file {$this->filename} already exists.";
				return false;
			}

			$tmp_file = $this->temp_path;
			// $upload_dir = $this->upload_dir;
			// $target_file = $this->filename;

			// attempt file upload
			if(move_uploaded_file($tmp_file, $target_path)) { 
				if($this->create()) {
					unset($this->temp_path); 
					return true; 
				}
			} else {
				$this->errors[] = "The file uploaded failed, possibly due to permissions on the update folder.";
				return false;
			}

		}

	}


	public function destroy() {
		// remove the db entry
		if ($this->delete()) {
			// then remove the file
			$target_path = $this->image_path();
			return unlink($target_path) ? true : false;
		} else {
			return false;
		}
	}

	public function image_path() {
		return $this->upload_dir.'/'.$this->filename;
	}

	public function web_path() {
		return $this->web_path.'/'.$this->filename;
	}

	public function web_link() {
		return WEB_PATH.'/view/'.$this->id;
	}

	public function nice_size() {
		if ($this->size < 1024) {
			return "{$this->size} bytes";
		} elseif ($this->size < 1048576) {
			$sizekb = round($this->size/1024);
			return "{$sizekb} kb";
		} else {
			$sizekb = round($this->size/1048576, 1);
			return "{$sizekb} mb";
		}
	}

	public function comments() {
		$model    = $this->comment_model;
		$comments = $model->find_comments_on($this->id);
		return $comments;
	}




}
