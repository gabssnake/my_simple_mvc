<?php

class User extends ActiveRecord {

	protected $table_name = "users";
	protected $db_fields  = array('id','username','password','first_name','last_name');

	public $id;
	public $username;
	public $password;
	public $first_name;
	public $last_name;

    public function __construct(Database $db) {
        parent::__construct($db);
    }

    // just a test for specific function
	public function full_name() {
		if (isset($this->first_name) && isset($this->last_name)) {
			return $this->first_name . " " . $this->last_name;
		} else {
			return "";
		}
	}


}