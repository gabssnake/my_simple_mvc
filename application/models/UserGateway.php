<?php

class UserGateway extends TableGateway {

	protected $table_name   = "users";
	protected $record_class = 'User';

    public function __construct(Database $db) {
        parent::__construct($db);
    }

    // verify if user is registered
	public function authenticate($username="", $password="") {
		$username= $this->db->escape_value($username);
		$password= $this->db->escape_value($password);
		$sql  = "SELECT * FROM users ";
		$sql .= "WHERE username = '{$username}' ";
		$sql .= "AND password = '{$password}' ";
		$sql .= "LIMIT 1";
		$result_array = $this->find_by_sql($sql);
		return !empty($result_array) ? array_shift($result_array) : false;
	}


}