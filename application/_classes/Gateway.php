<?php

class Gateway {

    protected $db;
    protected $table;
    protected $fields;

    public function __construct(Database $db, $table="", $fields=array()) {
        $this->db     = $db;
        $this->table  = $table;
        $this->fields = $fields;
    }



    public function findAll() {
        $query = "SELECT * FROM {$this->table}";
        return $this->db->query($query);
    }

    public function find($id) {
        $query = "SELECT * FROM {$this->table} WHERE id=? LIMIT 1";
        return $this->db->query($query, array($id));
    }

    public function findBy($data, $like=false) {
        $where = array();
        foreach ($data as $field => &$value) {
            if(!$like) { $where[] = "{$field} = ?"; }
            else {
                $value = "%{$value}%";
                $where[] = "{$field} LIKE ?";
            }
        }
        $sql = sprintf(
            "SELECT * FROM {$this->table} WHERE %s",
            implode(' AND ', $where)
        );
        return $this->db->query($sql, $data);
    }





    public function delete($id) {
        $query = "DELETE FROM {$this->table} WHERE id=?";
        return $this->db->modify($query, array($id));
    }

    public function insert($data) {
        // separate keys and values
        $fields = implode(', ', array_keys($data));
        $values = array_values($data);
        // placeholders
        $phold = '';
        foreach ($values as $val) { $phold .= '?, '; }
        $phold = trim($phold, ', ');
        // build query
        $sql = "INSERT INTO {$this->table} ({$fields}) values ({$phold})";
        // returns item id
        return $this->db->modify($sql, $values);
    }

    public function update($id, $data=array()) {
        // compose query
        $fields = array();
        foreach ($data as $field => &$value) {
            $fields[] = "{$field} = ?";
        }
        $fields = implode(', ', $fields);
        // final query
        $sql = "UPDATE {$this->table} SET {$fields} WHERE id = {$id}";
        // returns item id
        return $this->db->modify($sql, $data);
    }


}





/*

// usage

// $db = new Database("localhost", "snippets", "root", "root");

$table = "entries";
$fields = array(
    'id'          => '',
    'title'       => '',
    'notes'       => '',
    'tags'        => '',
    'code1'       => '',
    'code2'       => '',
    'code3'       => '',
    'last_opened' => '' );

// $entries = new Gateway($db, $table, $fields);

// update record (returns item id)
// $data = array(
//      'title' => 'Fuckin json!',
//      'notes' => 'Some notes' );
// $entries->update(11, $data);

// find by id
// $result = $entries->find(11);

// delete
//$result = $entries->delete(110);

// find all
// $result = $entries->findAll();

// find by field value (where set)
// $result = $entries->findBy(array(
//  'status' => 'paid',
//  'amount' => '250'
// ));

// find by field value (like set)
// $result = $entries->findBy(array(
//  'title' => 'nother'
// ),true);

// create record (returns item id)
// $data = array(
//      'title' => 'Mel Gibson',
//      'notes' => 'Some notes',
//      'tags'  => 'tag,tag,tag',
//      'code1' => 'yes indeed',
//      'code2' => 'lorem psum',
//      'code3' => 'dolor amet' );
// $id = $entries->insert($data);


// if (empty($result)) {
//  echo 'empty!';
// } else {
//  echo '<hr>'. count($result) .'<hr>';
//  echo '<pre>'; 
//  print_r($result); 
//  echo '</pre>';
// }


*/
