<?php


class Record {

    protected $db;
    protected $gateway;
    protected $data;

    public function __construct(Database $db, TableGateway $gateway, $data=array()) {
        $this->db = $db;
        $this->gateway = $gateway;
        $this->data = $data;
    }


}






/*

    protected $tableName = "entries";
    protected $fields  = array('id_entry','title','notes','tags','code1','code2','code3','date_created');

    protected function attributes() {
        // return an array of attribute keys and their values
        $attributes = array();
        foreach ($this->fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        $clean_attributes = array();
        // does not alter the actual value of attributes
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $this->db->escape_value($value);
        }
        return $clean_attributes;
    }

    private function has_attribute($attribute) {
        $object_vars = $this->attributes();
        return array_key_exists($attribute, $object_vars);
    }


    // $user = User::find_by_id(3);
    // $user->password = "hdiueouidhd";
    // $user->save();
    public function save() {
        // a new record wont have an id yet
        return isset($this->id) ? $this->update() : $this->create();
    }


    // $user = new User();
    // $user->username   = "johnsmith";
    // $user->password   = "abcd12345";
    // $user->first_name = "John";
    // $user->last_name  = "Smith";
    // $user->create();
    protected function create() {
        $attributes = $this->sanitized_attributes();
        $sql  = "INSERT INTO ". $this->tableName ." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($this->db->query($sql)) { 
            $this->id = $this->db->insert_id();
            return true; 
        } else { 
            return false; 
        }
    }

    
    // $user = User::find_by_id(3);
    // $user->password = "abcdefghij";
    // $user->update();
    protected function update() {
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql  = "UPDATE ". $this->tableName." SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=". $this->id;
        $db->query($sql);
        return ($this->db->affected()==1) ? true : false;
    }


    // $user = User::find_by_id(3);
    // $user->delete();
    public function delete() {
        $sql  = "DELETE FROM ". $this->tableName;
        $sql .= " WHERE id=". $this->db->escape_value($this->id);
        $sql .= " LIMIT 1";
        $this->db->query($sql);
        return ($this->db->affected_rows()==1) ? true : false;
    }
*/

