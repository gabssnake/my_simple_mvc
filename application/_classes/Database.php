<?php

class Database {
    
    protected $conn;
    protected $lastid;
    protected $affected;
    protected $lastquery;

    public function __construct($host, $db, $user, $pass) {
        try {
            // database connection
            $this->conn = new PDO("mysql:host=$host;dbname=$db",$user,$pass);
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        } catch(PDOException $e) {
            die('Database connection error: ' .$e->getMessage());
        }

    }

    protected function prequery($sql, $data=array()) {

        try {
            // prepare statement
            $query = $this->conn->prepare($sql);
            // force an indexed array with 'array_values'
            $query->execute(array_values($data));
        } catch (PDOException $e) {
            die('Error executing query: ' .$e->getMessage());
        }

        //update tracking vars
        $this->lastquery = $sql;
        $this->affected  = $query->rowCount();
        $this->lastid    = $this->conn->lastInsertId();

        // return statement
        return $query;
    }


    // read only
    public function query($sql, $data=array()) {
        // request
        $query = $this->prequery($sql, $data);
        // return assoc array
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $result = $query->fetchAll();
        return $result;
    }


    // insert, update, delete
    public function modify($sql, $data=array()) {
        // request
        $query = $this->prequery($sql, $data);
        return array(
            'affected' => $this->affected(),
            'lastid'   => $this->lastid()
            );
    }    


    public function lastid() {
        return $this->lastid;
    }

    public function affected() {
        return $this->affected;
    }


    public function close() {   
        if(isset($this->conn)) {
            $close = $this->conn->close();
            unset($this->conn);
        }
    }



}
