<?php

class Member extends Controller {

	protected $db;
	protected $session;

	public function __construct($db, $session) {
		parent::__construct();
		$this->db = $db;
        $this->session = $session;
	}


    public function login() {
        // default message
        $feedback = 'Hello there, login to continue.';
    	// if form has been submitted
        if ( isset($_POST['submit']) ) {
            // clean input  
            $username = trim($_POST['username']);
            $password = trim($_POST['password']);
            // check db to see if user exists
            $model = new UserGateway($this->db);
            $found_user = $model->authenticate($username, $password);
            // login or fail
            if($found_user) {
                $this->session->login($found_user);
                $this->log_action('Login', "{$found_user->username} logged in.");
                $this->redirect_to('/admin');
            } else {
                $username = htmlentities($username);
                $feedback = 'Username/password combination incorrect';
                $this->session->message($feedback);
            }
        } else {
            // declare to avoid errors
            $username = '';
        }
        // prepare data to be passed into view
        $data = array(
            'username' => $username,
            'feedback' => $feedback
        );
        $this->load_view('login', $data);
    }


    public function logout() {
    	if($this->session->is_logged_in()) { 
            $this->session->logout();
            $this->session->message('You were successfully logged out.');
        }
        // either way go to login form
        $this->redirect_to("/admin/login");
    }



    // Bcrypt FTW
    // http://phpmaster.com/why-you-should-use-bcrypt-to-hash-stored-passwords/

    // encrypt with bcrypt (blowfish, cost 11)
    public function bcrypt($password) {
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
            return crypt($password, $salt);
        } else { return false; }
    }

    // decrypt
    public function verify($password, $hashedPassword) {
        return crypt($password, $hashedPassword) == $hashedPassword;
    }


}