<?php

class Gallery extends Controller {

	protected $db;
	protected $session;

	public function __construct($db, $session) {
		parent::__construct();
		$this->db = $db;
        $this->session = $session;
	}

    public function all() {
    	$model  = new PhotoGateway($this->db);
		$photos = $model->find_all();
		// update message if there is one
        $feedback = $this->session->message();
		// prepare data to be passed into view
		$data = array(
			'photos' => $photos,
			'thumb_size' => 150,
			'total_found' => count($photos),
			'feedback' => $feedback
		);
		$this->load_view('home', $data);
    }


    public function details($id) {
		if (empty($id)) {
			$this->session->message("No photo id was provided.");
			$this->redirect_to("/");
		}
		// get the photo to show
		$model = new PhotoGateway($this->db);
		$photo = $model->find_by_id($id);
		if (!$photo) {
			$this->session->message("The photo could not be located.");
			$this->redirect_to("/");
		}
		// update message if there is one
        $feedback = $this->session->message();
		// prepare data to be passed into view
		$data = array(
			'photo'    => $photo,
			'path'     => $photo->web_path(),
			'comments' => $photo->comments(),
			'feedback' => $feedback
		);
		$this->load_view('details', $data);

    }

    // make a comment
    public function comment($id) {
		if (isset($_POST['submit'])) {
			$author = trim($_POST['author']);
			$body   = trim($_POST['comment']);

			$model = new CommentGateway($this->db);
			$new_comment = $model->make($id, $author, $body);

			if ($new_comment && $new_comment->save()) {
				// comment was posted
				$this->session->message("Your comment was posted.");
			} else {
				// pass session message
				$this->session->message("There was an error that prevented the comment from being solved.");
			}

			// reload the page to clear the post values and have a clean form
			$this->redirect_to("/view/{$id}");
		}
    }


}