<?php

class Logger extends Controller {

	protected $db;
	protected $session;
	protected $logfile;

	public function __construct($db, $session) {
		parent::__construct();
		$this->db = $db;
        $this->session = $session;
		$this->logfile = APP_PATH.'/application/logs/log.txt';
	}

	public function show_log() {
		// if(!$session->is_logged_in()) { redirect_to("login.php"); }

		$log = $this->logfile;
		$logs = array();
		$feedback = '';

		// read file
		if( file_exists($log) && is_readable($log) && $handle = fopen($log, 'r')) {
			// store each line in our array
			while(!feof($handle)) {
				$entry = fgets($handle);
				if(trim($entry) != "") { $logs[] = $entry; }
			}
			fclose($handle);
		} else {
			$feedback = "Could not read from {$log}.";
		}

		// prepare data to be passed into view
		$data = array(
			'logs'  => $logs,
			'feedback' => $feedback
		);
		$this->load_view('log', $data);
	}


	public function clear_log() {
		$log = $this->logfile;
		if( file_exists($log) && is_readable($log) ) {
			file_put_contents($this->logfile, '');
		}
		$this->log_action("Log cleared", "By {$this->session->user_id}");
		$this->redirect_to('/admin/log');
	}



}