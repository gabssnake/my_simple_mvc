<?php

class Backend extends Controller {

	protected $db;
	protected $session;

	public function __construct($db, $session) {
		parent::__construct();
		$this->db = $db;
        $this->session = $session;
	}

	protected function check_session() {
		if(!$this->session->is_logged_in()) {
			// reject user if not logged in
			$this->redirect_to("/admin/login");
		}
	}

	public function main() {
		$this->check_session();
		// load view
		$this->load_view('admin');
	}

	public function library() {
		$this->check_session();

		$model  = new PhotoGateway($this->db);
		$photos = $model->find_all();

		// update message if there is one
        $feedback = $this->session->message();

		// prepare data to be passed into view
		$data = array(
			'photos'      => $photos,
			'thumb_size'  => 150,
			'total_found' => count($photos),
			'feedback'    => $feedback
		);
		$this->load_view('library', $data);
	}

	public function comments($id) {
		$this->check_session();
		if (empty($id)) {
			$this->session->message("No photo id was provided.");
			$this->redirect_to('/admin/library');
		}
		$model = new PhotoGateway($this->db);
		$photo = $model->find_by_id($id);
		if (!$photo) {
			$this->session->message("The comments could not be loaded.");
			$this->redirect_to('/admin/library');
		}
		// get comments from db
		$comments = $photo->comments();
		// update message if there is one
        $feedback = $this->session->message();
		// prepare data to be passed into view
		$data = array(
			'comments' => $comments,
			'feedback' => $feedback
		);
		$this->load_view('comments', $data);
	}

	public function delete_photo($id) {
		$this->check_session();
		if (empty($id)) {
			$this->session->message("Could not delete, no photo ID was provided.");
			$this->redirect_to('/admin/library');
		}
		$model = new PhotoGateway($this->db);
		$photo = $model->find_by_id($id);
		if ($photo && $photo->destroy()) {
			$this->session->message("The photo {$photo->filename} was deleted.");
		} else {
			$this->session->message("Unknown error. The photo could not be deleted.");
		}
		$this->redirect_to('/admin/library');
	}

	public function delete_comment($id) {
		$this->check_session();
		if (empty($id)) {
			$this->session->message("Could not delete, no comment ID was provided.");
			$this->redirect_to('library.php');
		}
		$model   = new CommentGateway($this->db);
		$comment = $model->find_by_id($id);
		if ($comment && $comment->delete()) {
			$this->session->message("The comment was deleted.");
			$this->redirect_to("/admin/comments/{$comment->photograph_id}");
		} else {
			$this->session->message("The comment could not be deleted.");
			$this->redirect_to('/admin/library');
		}
	}

	public function upload() {
		$this->check_session();
		// process form if it was sent
		if (isset($_POST['submit'])) {
			$caption = $_POST['caption'];
			$photo = new Photo($this->db);
			$photo->caption = $caption;
			$photo->attach_file($_FILES['file_upload']);
			if ($photo->save()) {
				$this->session->message("Photograph uploaded succesfully.");
				$this->redirect_to('/admin/library');
			} else {
				$feedback = join("<br>", $photo->errors);
			}
		} else {
			$caption  = '';
			$feedback = '';
		}
		// print form
		$data = array(
			'caption'  => $caption,
			'feedback' => $feedback,
			'max_size' => 10805760 // 10mb
		);
		$this->load_view('upload', $data);
	}


}