<?php

include 'header.php';

echo $feedback ? '<p>'.$feedback.'</p>' : '';

echo '<p><a href="'.WEB_PATH.'/admin/library">Back to image list</a></p>';

// output comments
$output = '<div class="comments">';
if (!empty($comments)) {
	$output .= '<h3>Comments</h3>';
	$output .= '<ul class="comment-list">';
	foreach ($comments as $comment) {
		$output .= "<li><p>".strip_tags($comment->body, '<strong><em><p>')."</p>";
		$output .= "<p>Posted by <strong><em>".htmlentities($comment->author)."</em></strong>";
		$output .= " on ".$comment->created."</p>";
		$output .= '<p><a href="'.WEB_PATH.'/admin/comment/delete/'.$comment->id.'">Delete comment</a></p></li>';
	}
	$output .= '</ul>';
} else {
	$output .= '<p>There are no comments for this entry.</p>';
}
echo $output .= '</ul></div>';


include 'footer.php';

