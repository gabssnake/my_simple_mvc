<?php


include 'header.php';

echo $feedback ? '<p>'.$feedback.'</p>' : '';

echo '<p><a href="'.WEB_PATH.'/admin">Back to index</a></p>';
echo '<p><a href="'.WEB_PATH.'/admin/upload">Upload a new image</a></p>';


$output  = "<h1>{$total_found} images where found</h1>";
$output .= '<table class="image-list">';

foreach ($photos as $photo) {
	$output .= '<tr class="image'.$photo->id.'">';
	$output .= '<td><img src="'.$photo->web_path().'" ';
	$output .= 'width="'.$thumb_size.'" height="'.$thumb_size.'"></td>';
	$output .= '<td>'. $photo->nice_size() .'</td>';
	$output .= '<td>'. $photo->type .'</td>';
	$output .= '<td>'. $photo->caption .'</td>';
	$output .= '<td><a href="'.WEB_PATH.'/admin/comments/'.$photo->id.'">';
	$output .= count($photo->comments()).' comments</a></td>';
	$output .= '<td><a href="'.WEB_PATH.'/admin/delete/'.$photo->id.'">Delete</a></td>';
	$output .= '</tr>';
}
echo $output .= "</table>";


include 'footer.php';

