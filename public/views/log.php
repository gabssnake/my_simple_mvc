<?php

include 'header.php'; 

?>

<h1>Log file</h1>

<p><a href="<?php echo WEB_PATH.'/admin'; ?>">Back to index</a></p>

<p><a href="<?php echo WEB_PATH.'/admin/log/clear'; ?>">Clear Log File</a></p>

<?php

	// show feedback if present
	echo $feedback ? '<p>'.$feedback.'</p>' : '';

	// print the log list
	if (!empty($logs)) {
		$output = '<ul class="log-entries">';
		foreach ($logs as $entry) {
			$output .= "<li>{$entry}</li>";
		}
		echo $output .= '</ul>';
	} else {
		echo '<p>No entries were found.</p>';
	}


include 'footer.php';

