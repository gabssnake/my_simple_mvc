<?php
include 'header.php';
?>


<h1>Admin area</h1>

<p><a href="<?php echo WEB_PATH; ?>/admin/library">See all images in library</a></p>

<p><a href="<?php echo WEB_PATH; ?>/admin/upload">Upload a new image</a></p>

<p><a href="<?php echo WEB_PATH; ?>/admin/log">See the logfile</a></p>

<p><a href="<?php echo WEB_PATH; ?>/admin/logout">Sign Out</a></p>


<?php 
include 'footer.php';
