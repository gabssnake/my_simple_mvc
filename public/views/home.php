<?php



include 'header.php';

echo $feedback ? '<p>'.$feedback.'</p>' : '';

$output  = "<h1>{$total_found} images where found</h1>";
$output .= '<ul class="image-list">';
foreach ($photos as $photo) {
	$output .= '<li class="image'.$photo->id.'">';
	$output .= '<a href="'.$photo->web_link().'">';
	$output .= '<img src="'.$photo->web_path().'" ';
	$output .= 'width="'.$thumb_size.'" height="'.$thumb_size.'"';
	$output .= 'alt="'. $photo->caption .' - '. $photo->nice_size() .'">';
	$output .= '</a></li>';
}
echo $output .= "</ul>";

echo '<p><a href="'.WEB_PATH.'/admin">Go to Admin area</a></p>';

include 'footer.php';



