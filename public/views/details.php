<?php

include 'header.php';
?>

<p><a href="<?php echo WEB_PATH.'/'; ?>">Back to listing</a></p>

<?php echo $feedback ? '<p>'.$feedback.'</p>' : ''; ?>

<h1><?php echo $photo->filename; ?></h1>

<div class="image<?php echo $photo->id; ?>">
	<img src="<?php echo $path; ?>"
	 alt="<?php echo $photo->caption; ?>">
	<p>
		<?php 
		echo $photo->caption.' - '.$photo->nice_size(); 
		?>
	</p>
</div>


<div class="comments">

	<!-- list comments -->
	<?php
		if (!empty($comments)) {
			$output = '<h3>Comments</h3>';
			$output .= '<ul class="comment-list">';
			foreach ($comments as $comment) {
				$output .= "<li><p>".strip_tags($comment->body, '<strong><em><p>')."</p>";
				$output .= "<p>Posted by <strong><em>".htmlentities($comment->author)."</em></strong> on ".$comment->created."</p></li>";
			}
			$output .= '</ul>';
		} else {
			$output = '<p>There are no comments for this entry.</p>';
		}
		echo $output;
	?>
	
	<hr>

	<!-- new comment -->

	<h3>New comment</h3>

	<form action="<?php echo WEB_PATH.'/comment/'.$photo->id; ?>" method="post">

		<p>
			<label for="author">Your name:</label>
			<input type="text" name="author" value="" placeholder="Your name">
		</p>
		<p>
			<label for="comment">Your name:</label>
			<textarea name="comment" placeholder="Some comment"></textarea>
		</p>
		<p>
			<input type="submit" name="submit" value="Post comment">
		</p>

	</form>

</div>




<?php
include 'footer.php';

