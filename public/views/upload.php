<?php

include 'header.php';

?>

<h1>Test page</h1>

<?php echo $feedback ? '<p>'.$feedback.'</p>' : ''; ?>

<p><a href="<?php echo WEB_PATH; ?>/admin">Back to index</a></p>
<p><a href="<?php echo WEB_PATH; ?>/admin/library">See all images in library</a></p>

<form action="<?php echo WEB_PATH; ?>/admin/upload" enctype="multipart/form-data" method="post">
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_size; ?>">
	<input type="file" name="file_upload">
	<p>
		<label for="caption">Caption: </label>
		<input type="text" name="caption" value="<?php echo $caption; ?>" placeholder="Some text...">
	</p>
	<input type="submit" name="submit" value="Upload">
</form>



<?php

include 'footer.php';

